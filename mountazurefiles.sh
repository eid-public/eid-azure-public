#!/bin/bash
# $1 = Azure storage account name
# $2 = Azure storage account key
# $3 = Azure file share name
# $4 = mountpoint path


# update package lists
apt-get -y update

# install cifs-utils and mount file share
apt-get install cifs-utils chrony -y
mkdir $4
mkdir $42

echo "//$1.file.core.windows.net/$3  $4   cifs    vers=3.0,username=$1,password=$2,dir_mode=0700,file_mode=0700,serverino,x-systemd.automount 0  0" >> /etc/fstab 
echo "//$12.file.core.windows.net/$3  $42   cifs    vers=3.0,username=$12,password=xU9olPK216sjnPM1mE0ExMsmKcvoYwtbvcrJ5BlN/vdKJDWwPy1KfHDhhOhCWU5X2XPOQTmBsYf0uMN6DfP/Mg==,dir_mode=0777,file_mode=0777,serverino,x-systemd.automount 0  0" >> /etc/fstab 

mount $4 
mount $42 

#Disable docker install service
systemctl disable dcos-docker-install
systemctl stop dcos-docker-install
rm /etc/systemd/system/sockets.target.wants/docker.socket
rm /etc/systemd/system/docker.socket
rm /etc/systemd/system/docker.service.d -rf
rm /etc/systemd/system/multi-user.target.wants/docker.service
rm /etc/systemd/system/dcos-docker-install.service

#Install docker 20
sleep 30
apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce -y
apt-get upgrade -y
apt-get autoremove -y 
systemctl enable docker

#Reboot system
shutdown -r 1
